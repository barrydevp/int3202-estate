from django.db import models
from django.contrib.postgres.search import SearchVectorField, SearchVector
from datetime import datetime
from realtors.models import Realtor

class Listing(models.Model):
  realtor = models.ForeignKey(Realtor, on_delete=models.DO_NOTHING)
  title = models.CharField(max_length=200)
  address = models.CharField(max_length=200)
  city = models.CharField(max_length=100)
  state = models.CharField(max_length=100)
  zipcode = models.CharField(max_length=20)
  description = models.TextField(blank=True)
  price = models.DecimalField(max_digits=10, decimal_places=1)
  bedrooms = models.IntegerField()
  bathrooms = models.IntegerField()
  garage = models.IntegerField(default=0)
  sqft = models.IntegerField()
  lot_size = models.DecimalField(max_digits=5, decimal_places=1)
  preview = models.CharField(max_length=200, default="https://staticfile.batdongsan.com.vn/images/no-image.png")
  photo_1 = models.CharField(max_length=200, blank=True)
  photo_2 = models.CharField(max_length=200, blank=True)
  photo_3 = models.CharField(max_length=200, blank=True)
  photo_4 = models.CharField(max_length=200, blank=True)
  photo_5 = models.CharField(max_length=200, blank=True)
  photo_6 = models.CharField(max_length=200, blank=True)
  is_published = models.BooleanField(default=True)
  list_date = models.DateTimeField(default=datetime.now, blank=True)
  expired_at = models.DateTimeField(blank=True, null=True)
  ts_title_description = SearchVectorField(null=True, editable=False)

  def __str__(self):
    return self.title
