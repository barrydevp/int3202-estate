from datetime import datetime
from django.db import models
from django.contrib.auth.models import User

class Realtor(models.Model):
  user = models.ForeignKey(User, default=1, on_delete=models.CASCADE)
  photo = models.CharField(max_length=200, default="https://usercontent.one/wp/adtpest.com/wp-content/uploads/2018/08/default-avatar.jpg")
  description = models.TextField(blank=True)
  phone = models.CharField(max_length=20)
  is_mvp = models.BooleanField(default=False)
  credit = models.DecimalField(default=0, max_digits=10, decimal_places=2)

  def __str__(self):
    return self.user.username
