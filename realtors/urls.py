from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='realtor_listings_index'),
    path('<int:listing_id>', views.listing, name='reltor_listings'),
    path('search', views.search, name='realtor_listings_search'),
    path('listings/create', views.create_listing, name='realtor_listings_create'),
]
