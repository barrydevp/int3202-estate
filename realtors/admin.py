from django.contrib import admin

from .models import Realtor

class RealtorAdmin(admin.ModelAdmin):
  list_display = ('id', 'user', 'phone', 'credit')
  list_display_links = ('id', 'user')
  search_fields = ('user',)
  list_per_page = 25

admin.site.register(Realtor, RealtorAdmin)
