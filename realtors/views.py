from django.shortcuts import get_object_or_404, render, redirect
from django.contrib import messages, auth
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.contrib.postgres.search import SearchQuery
from listings.choices import price_choices, bedroom_choices, state_choices
from datetime import datetime
from django.contrib.postgres.search import SearchVectorField, SearchVector

from listings.models import Listing
from .models import Realtor

def index(request):
  if not request.user.is_authenticated:
    return redirect('/accounts/login')

  user_id = request.user.id
  realtor = Realtor.objects.get(user=user_id)

  listings = Listing.objects.order_by('-list_date').filter(realtor=realtor.id)

  paginator = Paginator(listings, 30)
  page = request.GET.get('page')
  paged_listings = paginator.get_page(page)

  context = {
    'user': request.user,
    'listings': paged_listings,
  }

  return render(request, 'realtors/index.html', context)

def listing(request, listing_id):
  listing = get_object_or_404(Listing, pk=listing_id)

  context = {
    'listing': listing
  }

  return render(request, 'listings/listing.html', context)

def search(request):
  queryset_list = Listing.objects.order_by('-list_date')

  # Keywords
  if 'keywords' in request.GET:
    keywords = request.GET['keywords']
    if keywords:
      queryset_list = queryset_list.filter(ts_title_description=SearchQuery(keywords))

  # City
  if 'city' in request.GET:
    city = request.GET['city']
    if city:
      queryset_list = queryset_list.filter(city__iexact=city)

  # State
  if 'state' in request.GET:
    state = request.GET['state']
    if state:
      queryset_list = queryset_list.filter(state__iexact=state)

  # Bedrooms
  if 'bedrooms' in request.GET:
    bedrooms = request.GET['bedrooms']
    if bedrooms:
      queryset_list = queryset_list.filter(bedrooms__lte=bedrooms)

  # Price
  if 'price' in request.GET:
    price = request.GET['price']
    if price:
      queryset_list = queryset_list.filter(price__lte=price)

  paginator = Paginator(queryset_list, 30)

  if 'page' in request.GET:
    page = request.GET['page']
    paged_listings = paginator.get_page(int(page))
  else:
    paged_listings = paginator.get_page(1)

  context = {
    'state_choices': state_choices,
    'bedroom_choices': bedroom_choices,
    'price_choices': price_choices,
    'listings': paged_listings,
    'values': request.GET
  }

  return render(request, 'listings/search.html', context)

def create_listing(request):
  if not request.user.is_authenticated:
    return redirect('/accounts/login')

  if request.method == 'POST':
    # Get form values
    title = request.POST['title']
    address = request.POST['address']
    city = request.POST['city']
    state = request.POST['state']
    zipcode = request.POST['zipcode']
    description = request.POST['description']
    price = float(request.POST['price'])
    bedrooms = int(request.POST['bedrooms'])
    bathrooms = int(request.POST['bathrooms'])
    garage = int(request.POST['garage'])
    sqft = int(request.POST['sqft'])
    preview = request.POST['preview']
    photo_1 = request.POST['photo_1']
    photo_2 = request.POST['photo_2']
    photo_3 = request.POST['photo_3']
    photo_4 = request.POST['photo_4']
    photo_5 = request.POST['photo_5']
    photo_6 = request.POST['photo_6']

    user_id = request.user.id
    realtor = Realtor.objects.get(user=user_id)

    listing = Listing(realtor_id=realtor.id, title=title, address=address, city=city, state=state, zipcode=zipcode, description=description, price=price, bedrooms=bedrooms, bathrooms=bathrooms, garage=garage, sqft=sqft, lot_size=0, preview=preview, photo_1=photo_1, photo_2=photo_2, photo_3=photo_3, photo_4=photo_4, photo_5=photo_5, photo_6=photo_6, is_published=True, list_date=datetime.now(), expired_at=datetime.now())

    (realtor, title, address, city, state, zipcode, description, price, bedrooms, bathrooms, garage, sqft, 0, preview, photo_1, photo_2, photo_3, photo_4, photo_5, photo_6, True, datetime.now())

    try:
      # listing.save(update_fields=['realtor_id', 'title', 'address', 'city', 'state', 'zipcode', 'description', 'price', 'bedrooms', 'bathrooms', 'garage', 'sqft', 'lot_size', 'preview', 'photo_1', 'photo_2', 'photo_3', 'photo_4', 'photo_5', 'photo_6', 'is_published', 'list_date'])
      listing.save()
    except Exception as e:
      messages.error(request, str(e))
      return redirect('realtor_listings_create')

    messages.success(request, 'created')
    return redirect('/listings/' + str(listing.id))
  else:
    return render(request, 'accounts/post.html')
