from django.shortcuts import render
from django.http import JsonResponse

# Create your views here.
def index(request):
    return JsonResponse({
        "success": True,
        "data": "Hi i'm estate_app API!"
    })
