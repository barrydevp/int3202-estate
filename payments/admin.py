from django.contrib import admin

from .models import Payment

class PaymentAdmin(admin.ModelAdmin):
  list_display = ('id', 'sender', 'receiver', 'note', 'amount', 'created_at')
  list_display_links = ('id',)
  list_filter = ('sender', 'receiver', 'note', 'amount')
  search_fields = ('sender', 'receiver', 'note', 'amount', 'created_at')
  list_per_page = 25

admin.site.register(Payment, PaymentAdmin)
