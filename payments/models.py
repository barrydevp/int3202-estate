from django.db import models
from datetime import datetime
from django.contrib.auth.models import User
from realtors.models import Realtor

class Payment(models.Model):
  sender = models.ForeignKey(Realtor, on_delete=models.DO_NOTHING, related_name='sender')
  receiver = models.ForeignKey(Realtor, on_delete=models.DO_NOTHING, related_name='receiver')
  note = models.TextField(default='', blank=True)
  amount = models.DecimalField(default=0, max_digits=10, decimal_places=2)
  created_at = models.DateTimeField(default=datetime.now)

  def __str__(self):
    return self.note
